/***
 *                    _____
 *                   / ____|
 *   __ _  ___ _ __ | |     ___  _   ___   __
 *  / _` |/ _ \ '_ \| |    / _ \| | | \ \ / /
 * | (_| |  __/ | | | |___| (_) | |_| |\ V /
 *  \__, |\___|_| |_|\_____\___/ \__,_| \_/
 *   __/ |
 *  |___/                           v 3.5
 *
 *  v.corlaix@gmail.com
 */


    /*** Des tonnes de variables ***/
var resultats = new Array;
var nomAuteur = "";
var policeAleat;
var polices = new Array('Nunito', 'Lato', 'Chau Philomene One', 'Roboto Condensed', 'Montserrat Alternates');
var nbCouls;
var randCouls;
var sexe = true;
var template = "";
var templates = new Array("pacte_nord", "faulio", "grallimard", "jlai_lu", "cachette");
var tableMois = new Array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");

var auteurPh;
var titrePh;
var urlPh;

var tableRef = new Array();
var URLtexte;
var imgCover;
var imgEdit;
var nbCovers = 81;
var nbLogos = 14;

var etatMenu = false;

var boutonsActifs = false;

    /*** Du zonage ***/

var divR1, divR3, divR4, divTxtCrit, divTxtAvis, divLigne1;
var divLigne2, divEdition, divAutQuad, divTitQuad, divEdQuad;
var divEdTranche, divAuteur, divTitTranche, divTranche;
var divAutTranche, divLesAvis, divEditQuad, divImage;
var divDockImg, divQuadCouv, divTxtBio, divReboot;

    /*** Functions! Functions everywhere! ***/

function creerCouv() {
	divR1 = document.getElementById("r1");
	divR3 = document.getElementById("r3");
	divR4 = document.getElementById("r4");
	divTxtCrit = document.getElementById("txtCritique");
	divTxtAvis = document.getElementById("txtAvis");
	divLigne1 = document.getElementById("ligne1");
	divLigne2 = document.getElementById("ligne2");
	divEdition = document.getElementById("edition");
	divAutQuad = document.getElementById("auteurQuad");
	divTitQuad = document.getElementById("titreQuad");
	divEdQuad = document.getElementById("editionQuad");
	divEdTranche = document.getElementById("editionTranches");
	divAuteur = document.getElementById("auteur");
	divTitTranche = document.getElementById("titreTranches");
	divTranche = document.getElementById("tranche");
	divAutTranche = document.getElementById("auteurTranches");
	divLesAvis = document.getElementById("avis");
	divEditQuad =document.getElementById("lEditeurQuad");
	divImage = document.getElementById("lImage");
	divDockImg = document.getElementById("dockImg");
	divQuadCouv = document.getElementById("quadCouv");
	divTxtBio = document.getElementById("txtBio");
    divReboot = document.getElementById("reboote");
	
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
        xmlhttp2 = new XMLHttpRequest();
        xmlhttp3 = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp2 = new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp3 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("GET", "titres.json", false);
    xmlhttp.send();
    var jsondata = eval("(" + xmlhttp.responseText + ")");
    xmlhttp2.open("GET", "resumes.json", false);
    xmlhttp2.send();
    var jsondata2 = eval("(" + xmlhttp2.responseText + ")");
    xmlhttp3.open("GET", "avis.json", false);
    xmlhttp3.send();
    var jsondata3 = eval("(" + xmlhttp3.responseText + ")");

    policeAleat = Math.round(Math.random() * (polices.length - 1));
    var fontChoisie = "'" + polices[policeAleat] + "', sans-serif";

    nbCouls = jsondata.couleurs.length;
    randCouls = Math.round(Math.random() * (nbCouls - 1));

    /* Ordre du tableau "tableRef" :
     0 - Titre part 1 		8 - Lieu
     1 - Titre part 2 		9 - Profession
     2 - Éditeur part 1 	10 - Organisme
     3 - Éditeur part 2 	11 - Loisirs
     4 - Résumé phrase 1 	12 - Évocations
     5 - Résumé phrase 2 	13 - Type
     6 - Résumé phrase 3 	14 - Genre
     7 - Résumé phrase 4 	15 - Personnage

     16 - critique			17 - avis
     */

    chargeCouv();
    purgeBG();

    // On génère une nouvelle couverture
    tableRef = [];
    nomAuteur = (document.forms[0].elements[0].value).toLowerCase();
    template = selectionTemplate(document.forms[0].elements[3].selectedIndex);
    setActiveStyleSheet(template);

    sexe = document.forms[0].elements[1].checked ? true:false;

    tableRef.push(Math.round(Math.random() * (jsondata.titre1.length - 1))); // Titre 1
    tableRef.push(Math.round(Math.random() * (jsondata.titre2.length - 1))); // Titre 2

    tableRef.push(Math.round(Math.random() * (jsondata.editeur1.length - 1))); // Éditeur 1
    tableRef.push(Math.round(Math.random() * (jsondata.editeur2.length - 1))); // Éditeur 2

    tableRef.push(Math.round(Math.random() * (jsondata2.resume1.length - 1))); // Résumé 1
    tableRef.push(Math.round(Math.random() * (jsondata2.resume2.length - 1))); // Résumé 2
    tableRef.push(Math.round(Math.random() * (jsondata2.resume3.length - 1))); // Résumé 3
    tableRef.push(Math.round(Math.random() * (jsondata2.resume4.length - 1))); // Résumé 4

    tableRef.push(Math.round(Math.random() * (jsondata2.lieu.length - 1))); // Lieu
    tableRef.push(Math.round(Math.random() * (jsondata2.profession.length - 1))); // Profession
    tableRef.push(Math.round(Math.random() * (jsondata2.organisme.length - 1))); // Organisme
    tableRef.push(Math.round(Math.random() * (jsondata2.loisirs.length - 1))); // Loisirs
    tableRef.push(Math.round(Math.random() * (jsondata2.evocations.length - 1))); // Loisirs
    tableRef.push(Math.round(Math.random() * (jsondata2.type.length - 1))); // Type
    tableRef.push(Math.round(Math.random() * (jsondata2.genre.length - 1))); // Genre
    tableRef.push(Math.round(Math.random() * (jsondata2.perso.length - 1))); // Personnage

    tableRef.push(Math.round(Math.random() * (jsondata3.critique.length - 1))); // Genre
    tableRef.push(Math.round(Math.random() * (jsondata3.avis.length - 1))); // Personnage

    divR1.innerHTML = jsondata2.resume1[tableRef[4]] + " " + jsondata2.resume2[tableRef[5]];
    divR3.innerHTML = jsondata2.resume3[tableRef[6]];
    divR4.innerHTML = jsondata2.resume4[tableRef[7]];

    divTxtCrit.innerHTML = jsondata3.critique[tableRef[16]];
    divTxtAvis.innerHTML = "&laquo;&nbsp;" + jsondata3.avis[tableRef[17]] + "&nbsp;&raquo;";

    divLigne1.innerHTML = jsondata.titre1[tableRef[0]];
    divLigne2.innerHTML = jsondata.titre2[tableRef[1]];

    divEdition.innerHTML = "Éditions " + jsondata.editeur1[tableRef[2]] + " " + jsondata.editeur2[tableRef[3]];
    divAutQuad.innerHTML = "<span style='text-transform: capitalize;'>"+ nomAuteur +"</span>";
    divTitQuad.innerHTML = jsondata.titre1[tableRef[0]] + " " + jsondata.titre2[tableRef[1]];
    divEdQuad.innerHTML = "&copy; 2016, les éditions « " + jsondata.editeur1[tableRef[2]] + " " + jsondata.editeur2[tableRef[3]] + " »";
    divEdTranche.innerHTML = "<span class='centrage'>" + jsondata.editeur1[tableRef[2]] + "<br />" + jsondata.editeur2[tableRef[3]] + "</span>";

    var laCoul = jsondata.couleurs[randCouls];
    document.getElementById("couverture").style.display = "block";
    divReboot.style.display = "block";
    document.getElementById("formNom").style.display = "none";

    //URLtexte = encodeURIComponent('http://gen-couv.nootilus.com?nom=' + nomAuteur + '&ref=' + tableRef + '&sx=' + sexe);
    document.forms[0].elements[0].value = nomAuteur;


    /****** Éléments variants selon la maquette choisie ******/

    switch (template) {
        case "pacte_nord" :
            var laBio = "<span style='font-style:none; font-weight:bold; color:#999;'>Qui est l'auteur ?</span><br />" +
                "<span style='text-transform: capitalize;'>" + nomAuteur + "</span> est " + jsonSexe("né/née") +
                " le " + getRandomDate() + " à " + jsondata2.lieu[tableRef[8]] + ". " + jsonSexe(jsondata2.profession[tableRef[9]]) + " au sein " +
                jsondata2.organisme[tableRef[10]] + ", " + jsonSexe("il/elle") + " consacre ses loisirs à " + jsonSexe(jsondata2.loisirs[tableRef[11]]) +
                ". L'écriture lui permet de " + jsonSexe(jsondata2.evocations[tableRef[12]]) +
                ". " + jsonSexe("Il/Elle") + " travaille actuellement à " + jsondata2.type[tableRef[13]] + " " + jsondata2.genre[tableRef[14]] +
                " mettant en scène " + jsonSexe(jsondata2.perso[tableRef[15]]) + ".";

            divLigne1.style.fontFamily = fontChoisie;
            divLigne2.style.fontFamily = fontChoisie;
            divAuteur.style.fontFamily = fontChoisie;
            divAutQuad.style.fontFamily = fontChoisie;
            divTitQuad.style.fontFamily = fontChoisie;
            divTitTranche.style.fontFamily = fontChoisie;
            divAuteur.innerHTML = "<span style='text-transform: capitalize;'>"+ nomAuteur +"</span>";
            divAuteur.style.backgroundColor = laCoul;
            divLigne1.style.backgroundColor = laCoul;
            divLigne2.style.backgroundColor = laCoul;
            divEdition.style.backgroundColor = laCoul;
            divEdQuad.style.backgroundColor = laCoul;
            divTranche.style.backgroundColor = laCoul;
            divTitTranche.innerHTML = "<span style='text-transform: capitalize;'>" + nomAuteur + "</span> - " +
            jsondata.titre1[tableRef[0]] + " " + jsondata.titre2[tableRef[1]];
            break;
        case "faulio" :
            var laBio = jsonSexe("Né/Née") + " le " + getRandomDate() + ", <span style='text-transform: capitalize;'>" + nomAuteur +
                "</span> a fait des études de <span style='text-transform:lowercase;'>" +
                jsonSexe(jsondata2.profession[tableRef[9]]) + "</span> avant de devenir écrivain. " + jsonSexe("Il/Elle") + " vit maintenant à " + jsondata2.lieu[tableRef[8]] +
                ", partageant son temps entre " + jsonSexe(jsondata2.loisirs[tableRef[11]]) + " et " + jsonSexe(jsondata2.evocations[tableRef[12]]) +
                ". Sa prochaine œuvre, qu’" + jsonSexe("il/elle") + " fera " + jsondata2.genre[tableRef[14]] + ", sera " + jsondata2.type[tableRef[13]] + " dont le personnage principal est " +
                jsonSexe(jsondata2.perso[tableRef[15]]) + ".";

            divLigne1.style.fontFamily = fontChoisie;
            divLigne2.style.fontFamily = fontChoisie;
            divAuteur.style.fontFamily = fontChoisie;
            divAutQuad.style.fontFamily = fontChoisie;
            divTitQuad.style.fontFamily = fontChoisie;
            divTitTranche.style.fontFamily = fontChoisie;
            divAuteur.innerHTML = "<span style='text-transform: capitalize;'>"+ nomAuteur +"</span>";
            divAutTranche.innerHTML = "<span style='text-transform: capitalize;' class='centrage'>" + nomAuteur + "</span>";
            divTitTranche.innerHTML = jsondata.titre1[tableRef[0]] + " " + jsondata.titre2[tableRef[1]];
            divLesAvis.style.backgroundColor = laCoul;
            divLesAvis.style.borderColor = laCoul;
            divR4.style.backgroundColor = laCoul;
            divR4.style.borderColor = laCoul;
            divTranche.style.backgroundColor = laCoul;
            break;
        case "grallimard" :
            var laBio = jsonSexe("Né/Née") + " le " + getRandomDate() + ", <span style='text-transform: capitalize;'>" + nomAuteur +
                    "</span> vit maintenant à " + jsondata2.lieu[tableRef[8]] +" en compagnie d’" +  jsonSexe(jsondata2.perso[tableRef[15]]) +
                    " avec qui "+ jsonSexe("il/elle") +" aime "+ jsonSexe(jsondata2.evocations[tableRef[12]]) + ". C'est son activité de " +
                    (jsonSexe(jsondata2.profession[tableRef[9]])).toLowerCase() +
                    " "+  jsondata2.organisme[tableRef[10]] +" qui l’a "+ jsonSexe("amené/amenée")+ " à l’écriture. Son objectif est de "+
                    jsonSexe(jsondata2.loisirs[tableRef[11]])+ ", mais "+ jsonSexe("il/elle") +" consacre en ce moment toute son énergie à "+
                    jsondata2.type[tableRef[13]] +" "+ jsondata2.genre[tableRef[14]];

            divLigne1.style.fontFamily = fontChoisie;
            divLigne2.style.fontFamily = fontChoisie;
            divAuteur.style.fontFamily = fontChoisie;
            divAutQuad.style.fontFamily = fontChoisie;
            divTitQuad.style.fontFamily = fontChoisie;
            divTitTranche.style.fontFamily = fontChoisie;
            divAuteur.innerHTML = "par "+ "<span style='text-transform: capitalize;'>"+ nomAuteur +"</span>";
            divEdition.style.backgroundColor = laCoul;
            divEditQuad.style.backgroundColor = laCoul;
            divImage.style.borderColor = laCoul;
            divTranche.style.backgroundColor = laCoul;
            divAutTranche.innerHTML = "<span style='text-transform: capitalize;'>" + nomAuteur + "</span>";
            divTitTranche.innerHTML = "<span style='text-transform: capitalize;'>" + nomAuteur + "</span> - " +
                jsondata.titre1[tableRef[0]] + " " + jsondata.titre2[tableRef[1]];
            break;
        case "jlai_lu" :
            var laBio = jsonSexe("Installé/Installée") +" depuis "+getRandomAnnee() +" à "+ jsondata2.lieu[tableRef[8]] +
                    ", <span style='text-transform: capitalize;'>" + nomAuteur +"</span> y exerce la profession de "+
                (jsonSexe(jsondata2.profession[tableRef[9]])).toLowerCase() +" dans le cadre "+ jsondata2.organisme[tableRef[10]] +". "+
                jsonSexe("Il/Elle") +" partage son temps entre l’écriture et "+ jsonSexe(jsondata2.loisirs[tableRef[11]]) +
                    ", et compte bien mettre à profit sa nouvelle notoriété pour "+ jsonSexe(jsondata2.evocations[tableRef[12]])+
                    ". Sa rencontre avec "+ jsonSexe(jsondata2.perso[tableRef[15]]) +" l’a dernièrement "+ jsonSexe("inpsiré/inspirée")+
                    " pour travailler à "+ jsondata2.type[tableRef[13]] +" "+ jsondata2.genre[tableRef[14]] +
                    " qui devrait sortir dans le courant le l’année.";

            divLigne1.style.fontFamily = fontChoisie;
            divLigne2.style.fontFamily = fontChoisie;
            divAuteur.style.fontFamily = fontChoisie;
            divAutQuad.style.fontFamily = fontChoisie;
            divTitQuad.style.fontFamily = fontChoisie;
            divTitTranche.style.fontFamily = fontChoisie;
            divAuteur.innerHTML = "<span style='text-transform: capitalize;'>"+ nomAuteur +"</span>";
            divDockImg.style.backgroundColor = laCoul;
            divQuadCouv.style.backgroundColor = laCoul;
            divAutTranche.innerHTML = "<span style='text-transform: capitalize;'>" + nomAuteur + "</span>";
            divTitTranche.innerHTML = jsondata.titre1[tableRef[0]] + " " + jsondata.titre2[tableRef[1]];
            break;
        case "cachette" :
            var laBio = jsonSexe("Âgé/Âgée") +" de "+ getRandomAge() +" ans, <span style='text-transform: capitalize;'>" + nomAuteur
                +"</span> a quitté son "+ jsondata2.lieu[tableRef[8]] +" natal pour venir s’installer à la capitale. D’abord "+
                jsonSexe("employé/employée") +" "+ jsondata2.organisme[tableRef[10]] +" en tant que " +
                (jsonSexe(jsondata2.profession[tableRef[9]])).toLowerCase() +", "+ jsonSexe("il/elle")+
                " plaque tout pour embrasser pleinement sa carrière d’écrivain.<br />Ne boudant pas les relations publiques,"+
                jsonSexe("il/elle") +" aime "+ jsonSexe(jsondata2.loisirs[tableRef[11]]) +" devant ses invités "+
                "et discuter de sa volonté de "+ jsonSexe(jsondata2.evocations[tableRef[12]]) +".<br/>"+
                "Sa prochaine œuvre sera inspirée par "+ jsonSexe(jsondata2.perso[tableRef[15]]) +" qu’"+ jsonSexe("il/elle") +
                " mettra en scène dans "+ jsondata2.type[tableRef[13]] +" "+ jsondata2.genre[tableRef[14]] +".";

            divTranche.style.backgroundColor = "#f5e6c7";
            divDockImg.style.backgroundColor = "#f5e6c7";
            divQuadCouv.style.backgroundColor = "#f5e6c7";
            divImage.style.border = "5px solid #b91e22";

            divAuteur.innerHTML = "<span style='text-transform: capitalize;'>"+ nomAuteur +"</span>";
            divAutTranche.innerHTML = "<span style='text-transform: capitalize;'>" + nomAuteur + "</span>";
            divTitTranche.innerHTML = jsondata.titre1[tableRef[0]] + " " + jsondata.titre2[tableRef[1]];
            break;
    }

    /************************************************************/

    divTxtBio.innerHTML = laBio;
}

function menuOn(e) {
    e.style.left = "-5px";
    etatMenu = true;
}

function menuOff(e) {
    if (etatMenu) {
        e.style.left = "-205px";
        etatMenu = false;
    }
}

/*function nvelleMaquette() {
    var choixForm = document.getElementById("maquette2");
    var strUser = choixForm.options[choixForm.selectedIndex].text;
    console.log(strUser);

    setActiveStyleSheet(strUser);

    chargeCouv();
    purgeBG();

    divReboot.style.left = "-205px";
    etatMenu = false;
}*/

function jsonSexe(leMot) {
    var mascFem = leMot.split("/");
    if (!sexe && mascFem.length > 1) return mascFem[1];
    else return mascFem[0];
}

function chargeCouv() {
    /*** Charge une image au hasard dans mon propre stock ***/
    imgCover = "";
    var bgImg = "";
    var laPioche = Math.floor(Math.random() *nbCovers);
    imgCover = "images/couvs/couv" + laPioche + ".jpg";
    bgImg = '#7d7d7d url(' + imgCover + ') no-repeat center center / cover';
	divImage.style.background = bgImg;

    /*** Idem pour le picto éditeur ***/
    imgEdit = "";
    var bgEdit1 = bgEdit2 = "";
    var laPelle = Math.floor(Math.random() *nbLogos);
    imgEdit = "images/logos/logo" + laPelle + ".jpg";
    bgEdit1 = '<img style="width:75px; height:75px;" alt="" src="' + imgEdit + '"/>';
    bgEdit2 = '<img style="width:100px; height:100px;" alt="" src="' + imgEdit + '"/>';
    document.getElementById("imgEdTranche").innerHTML = bgEdit1;
    document.getElementById("imgEdQuad").innerHTML = bgEdit2;
}

function getRandomDate() {
    var mydate = new Date();
    var cetteAnnee = new Date();
    var lAge = Math.round(Math.random() * 60) + 20;
    var leJour;
    var leMois;
    var lAnnee;
    mydate.setTime(mydate.getTime() * Math.random());
    mydate.toLocaleString();
    leJour = mydate.getDay() + 1;
    if (leJour == 1) leJour = "1er";
    lAnnee = cetteAnnee.getFullYear() - lAge;
    leMois = tableMois[mydate.getMonth()];
    var laDate = leJour + " " + leMois + " " + lAnnee;
    return laDate;
}

function getRandomAnnee() {
    var cetteAnnee = new Date();
    var lAge = Math.round(Math.random() * 60) + 20;
    var lAnnee;
    lAnnee = cetteAnnee.getFullYear() - lAge;
    var laDate = lAnnee;
    return laDate;
}

function getRandomAge() {
    var lAge = Math.round(Math.random() * 60) + 20;
    return lAge;
}

function valider() {
    if (document.formule.nom.value == "") {
        alert("Saisissez un prénom et un nom, s’il vous plaît.");
        return false;
    } else {
        creerCouv();
        return false;
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null) return "";
    else resultats.push(decodeURIComponent(results[1].replace(/\+/g, " ")));
}

function reBoote() {
    window.location.reload(true);
}

function selectionTemplate(lequel) {
    /***
     * 0 - Pacte Nord
     * 1 - Faulio
     * 2 - Grallimard
     * 3 - J’l’ai Lu
     * 4 - Cachette
     * 5 - Random
     */

    if (lequel <= 4) {
        choix = templates[lequel];
    } else {
        var paf = Math.floor(Math.random() * 5);
        choix = templates[paf];
    }

    return choix;
}

function purgeBG() {
    divAuteur.style.backgroundColor = "transparent";
    divLigne1.style.backgroundColor = "transparent";
    divLigne2.style.backgroundColor = "transparent";
    divEdition.style.backgroundColor = "transparent";
    divEdQuad.style.backgroundColor = "transparent";
    divTranche.style.backgroundColor = "#000000";
    divLesAvis.style.backgroundColor = "transparent";
    divLesAvis.style.border = "none";
    divR4.style.backgroundColor = "transparent";
    divR4.style.border = "none";
    divEditQuad.style.backgroundColor = "transparent";
    divImage.style.border = "none";
    divDockImg.style.backgroundColor = "#000000";
    divQuadCouv.style.backgroundColor = "#000000";
}

/******************************* Switcheur de CSS ********************/
/** http://www.debian-fr.org/changer-dynamiquement-une-feuille-css-t22321.html **/

function setActiveStyleSheet(title) {
    var i, a, main;
    for (i = 0; (a = document.getElementsByTagName("link")[i]); i++) {
        if (a.getAttribute("rel").indexOf("style") != -1
            && a.getAttribute("title")) {
            a.disabled = true;
            if (a.getAttribute("title") == title) a.disabled = false;
        }
    }
}