/**
 * Created by nootilus on 17/08/14.
 */

function affTotal() {
    if (window.XMLHttpRequest) {
        xmlhttp=new XMLHttpRequest();
        xmlhttp2=new XMLHttpRequest();
        xmlhttp3=new XMLHttpRequest();
    } else {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp3=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("GET","titres.json",false);
    xmlhttp.send();
    var jsondata=eval("("+xmlhttp.responseText+")");
    xmlhttp2.open("GET","resumes.json",false);
    xmlhttp2.send();
    var jsondata2=eval("("+xmlhttp2.responseText+")");
    xmlhttp3.open("GET","avis.json",false);
    xmlhttp3.send();
    var jsondata3=eval("("+xmlhttp3.responseText+")");

    var totalTT1 = jsondata.titre1.length;
    var totalTT2 = jsondata.titre2.length;
    var totalED1 = jsondata.editeur1.length;
    var totalED2 = jsondata.editeur2.length;
    var totalRES1 = jsondata2.resume1.length;
    var totalRES2 = jsondata2.resume2.length;
    var totalRES3 = jsondata2.resume3.length;
    var totalRES4 = jsondata2.resume4.length;
    var totalLIEU = jsondata2.lieu.length;
    var totalPROF = jsondata2.profession.length;
    var totalORG = jsondata2.organisme.length;
    var totalLOIS = jsondata2.loisirs.length;
    var totalEVO = jsondata2.evocations.length;
    var totalTYPE = jsondata2.type.length;
    var totalGENRE = jsondata2.genre.length;
    var totalPERSO = jsondata2.perso.length;
    var totalCRIT = jsondata3.critique.length;
    var totalAVIS = jsondata3.avis.length;
	var nbCovers = 81;
	var nbLogos = 14;

    document.getElementById("titre").innerHTML = totalTT1 + totalTT2 + " demi morceaux de titres";
    document.getElementById("edit").innerHTML = totalED1 + totalED2 + " demi morceaux de maison d’édition";
    document.getElementById("resu12").innerHTML = totalRES1 + totalRES2 + " demi partie de résumé";
    document.getElementById("resu3").innerHTML = totalRES3 + " questionnement profond sur le texte";
    document.getElementById("resu4").innerHTML = totalRES4 + " éloges d’autopromotion";
    document.getElementById("lieu").innerHTML = totalLIEU + " lieux de résidence";
    document.getElementById("prof").innerHTML = totalPROF + " professions";
    document.getElementById("orga").innerHTML = totalORG + " lieux de travail";
    document.getElementById("lois").innerHTML = totalLOIS + " loisirs";
    document.getElementById("evo").innerHTML = totalEVO + " motivations à écrire";
    document.getElementById("type").innerHTML = totalTYPE + " ouvrages en cours";
    document.getElementById("genre").innerHTML = totalGENRE + " styles";
    document.getElementById("perso").innerHTML = totalPERSO + " personnages";
    document.getElementById("lesAvis").innerHTML = totalCRIT + " avis";
    document.getElementById("crit").innerHTML = totalAVIS + " critiques";
	document.getElementById("lesCouvs").innerHTML = nbCovers + " photos de couverture";
    document.getElementById("lesEds").innerHTML = nbLogos + " logos d’éditeurs";
}

function infoTotal() {
    if (window.XMLHttpRequest) {
        xmlhttp=new XMLHttpRequest();
        xmlhttp2=new XMLHttpRequest();
        xmlhttp3=new XMLHttpRequest();
    } else {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp3=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("GET","titres.json",false);
    xmlhttp.send();
    var jsondata=eval("("+xmlhttp.responseText+")");
    xmlhttp2.open("GET","resumes.json",false);
    xmlhttp2.send();
    var jsondata2=eval("("+xmlhttp2.responseText+")");
    xmlhttp3.open("GET","avis.json",false);
    xmlhttp3.send();
    var jsondata3=eval("("+xmlhttp3.responseText+")");

    /*console.log("totalTT1 = " + jsondata.titre1.length);
    console.log("totalTT2 = " + jsondata.titre2.length);
    console.log("totalED1 = " + jsondata.editeur1.length);
    console.log("totalED2 = " + jsondata.editeur2.length);
    console.log("totalRES1 = " + jsondata2.resume1.length);
    console.log("totalRES2 = " + jsondata2.resume2.length);
    console.log("totalRES3 = " + jsondata2.resume3.length);
    console.log("totalRES4 = " + jsondata2.resume4.length);
    console.log("totalLIEU = " + jsondata2.lieu.length);
    console.log("totalPROF = " + jsondata2.profession.length);
    console.log("totalORG = " + jsondata2.organisme.length);
    console.log("totalLOIS = " + jsondata2.loisirs.length);
    console.log("totalEVO = " + jsondata2.evocations.length);
    console.log("totalTYPE = " + jsondata2.type.length);
    console.log("totalGENRE = " + jsondata2.genre.length);
    console.log("totalPERSO = " + jsondata2.perso.length);
    console.log("totalCRIT = " + jsondata3.critique.length);
    console.log("totalAVIS = " + jsondata3.avis.length);*/
}